<?php
/**
 * @file
 * T2WP migration class.
 *
 * Tumblr's API XML export provides elements such as titles and slugs less consistently than Tumblr2WP,
 * and by default only provides a subset of posts at a time.
 * 
 * However, it provides distinct subfields for audio and video (captions, id3 tags).
 * It is possible to grab all post feeds (at 50 posts each) programmatically.
 * 
 * This command is one way, replacing {blog-shortname} and {total posts}:
 * curl "http://{blog-shortname}.tumblr.com/api/read?start=[0-{total-posts}:50]&num=50" -o "tumblr-api-{blog-shortname}-#1.xml"
 * 
 * You can cut and paste all but the header and closing RSS tags into the first file to get a single XML file.
 * 
 * Note: this class relies on the Media module for media handling.
 * If you use this class as-is, please enable the Media module.
 */
class TumblrMigrationAPI extends XMLMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

  // Migration description
  $this->description = t('XML feed of Tumblr posts from Tumblr API export feature');

  // Define XML file source and list of URLs
  $xml_folder = DRUPAL_ROOT . '/' . drupal_get_path('module', 'tumblrmigrate') . '/xml/';
  //$items_url = $xml_folder . '<xml name>;

  // Define item XPath, relative to document
  $item_xpath = '/tumblr/posts/post';
  // Define item source ID, relative to item_xpath
  $item_ID_xpath = '@id';

  // Fetch multiple files from xml folder
  $urls=array();
  foreach (glob($xml_folder . "/*.xml") as $filename) {
    array_push($urls, $filename);
  }

  /**
   * Define explicit fields from XML source, to be used in field mappings below.
   * Reference: https://dgtlmoon.com/drupal-migrate-with-file-field
   */
  $fields = array(
    'sourceid' => t('Post ID from Tumblr'),   
    'image' => t('Image file'),
    'image title' => t('Image title tag'),
    'image alt' => t('Image alt tag'),
    'image caption' => t('Image caption'),
    'audio' => t('Audio file'),
    'audio title' => t('Audio title'),
    // 'audio caption' => t('Audio caption'),
    'audio author' => t('Audio author'),
    'video' => t('Video file'),
    // 'video caption' => t('Video caption'),
    'video title' => t('Video caption'),
  );

  // Define source and destination
  $this->source = new MigrateSourceXML($urls, $item_xpath, $item_ID_xpath, $fields);
  // Destination: Article
  $this->destination = new MigrateDestinationNode('article');
    
  // A/V-only migration, updates existing nodes
  $this->systemOfRecord = Migration::DESTINATION;
  
  /**
   * The source ID here is the one retrieved from each data item in the XML file, and
   * used to identify specific items, this is the same structure as the DB fields api
   * Reference: drupal.org/node/1152156
   */
  $this->map = new MigrateSQLMap($this->machineName,
    array(
      'sourceid' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'description' => 'Post ID',
       'alias' => 'p',
      )
    ),
    MigrateDestinationNode::getKeySchema()
  );
  
  /**
   * Define field mappings.
   * XPath mappings:
   * addFieldMapping('destinationfield','sourcefield')->xpath('xpath');
   * Apply xpath to source; put value into the source row's 'sourcefield' field.
   * When mappings are processed, the source row's 'sourcefield' field
   *  goes into the destination node's 'destinationfield' field.
   * 
   * For example:
   *  $this->addFieldMapping('created', 'created_date')->xpath('/content/CreateDate');
   * 
   * References: drupal.org/node/1152156, https://drupal.la/articles/drupal-migrate-using-xml-in-0-to-35
   */

  // Set primary key for previous (WP) migration
  $this->addFieldMapping('nid', 'sourceid')->sourceMigration('tumblr2wp_xml');
  
  /**
   * Image mapping
   */
  // Test with a photoset containing individual captions; may need to use /photoset for 'field_image' and provide subfields in prepareRow()
  $this->addFieldMapping('field_image', 'image')->xpath('photoset/photo');
  $this->addFieldMapping('field_image:title', 'image title')->xpath('photoset/photo/@caption');
  $this->addFieldMapping('field_image:alt', 'image alt')->xpath('photoset/photo/@caption');
  $this->addFieldMapping('field_image:caption', 'image caption')->xpath('photoset/photo/@caption');
  $this->addFieldMapping('field_image:preserve_files')->defaultValue(FALSE);
  $this->addFieldMapping('field_image:file_replace')
       ->defaultValue(MigrateFile::FILE_EXISTS_REUSE);

  /**
   * Audio mapping
   */
  $this->addFieldMapping('field_audio', 'audio')->xpath('audio-player');
  $this->addFieldMapping('field_audio:description', 'audio title')->xpath('id3-title');
  $this->addFieldMapping('field_audio:caption', 'caption')->xpath('audio-caption');
  $this->addFieldMapping('field_audio:preserve_files')->defaultValue(FALSE);
  $this->addFieldMapping('field_authors', 'audio author')->xpath('id3-artist');
  // $this->addFieldMapping('NULL', 'audio title')->xpath('id3-title');
  // $this->addFieldMapping('NULL', 'video title')->xpath('id3-title');
 
 /**
  * Video mapping
  */
  $this->addFieldMapping('field_video', 'video')->xpath('video-player');
  // Using default file class - AsIs threw errors
  $this->addFieldMapping('field_video:preserve_files')->defaultValue(FALSE);
  
 /**
  * Documenting field mapping
  */
  // Unmapped destination fields
 $this->addFieldMapping('uid')
   ->issueGroup(t('DNM'));
 $this->addFieldMapping('body')
   ->issueGroup(t('DNM'));
 }
   
 /**
  * Implement a prepareRow method to manipulate the source row before saving
  */
 public function prepareRow($row) {
   $i = 0;
   // Extract image information from photosets 
   if (isset($row->xml->{'photoset'})) {
     foreach ($row->xml->{'photoset'}->{'photo'} as $photo) {
       // Set row elements, then adjust field mapping above using results
       $url = (string) $photo->{'photo-url'}[0];
       $caption = (string)$photo->attributes()->caption;
       $row->xml->{'photoset'}->{'photo'}[$i] = $url;
       $i++;
     }
   }

   $i = 0;
   /* Extract audio URL */
   foreach ($row->xml->{'audio-player'} as $audio) {
     $audioembed = (string) $audio;
     $start = strpos($audioembed, 'file=') + 5;
     $end = strpos($audioembed, 'mp3') + 3;
     $length = $end - $start;
     $url = urldecode(substr($audioembed, $start, $length));
     $row->xml->{'audio-player'}[$i] = $url;
     $i++;
   }

   /* Extract video URL */ 
   $i = 0;
   foreach ($row->xml->{'video-player'} as $video) {
     $videoembed = (string) $video;
     $start = strpos($videoembed, 'src="') + 5;
     $firstpart = substr($videoembed, $start, strlen($videoembed));
     $url = urldecode(substr($firstpart, 0, strpos($firstpart, '"')));
     $row->xml->{'video-player'}[$i] = $url;
     $i++;
   }
   return TRUE;
 }
  
 /**
  * Implement a prepare method to manipulate the node information before saving
  * Get image, audio, and video information.
  */
 public function prepare($node, stdClass $row) {
   // Get image info
   if (isset($row->xml->{'photoset'})) {   
     foreach ($node->field_image[LANGUAGE_NONE] as $image) {
       $alt = $image['alt'];
       $title = $image['title'];
       $caption = $title;
       $file = entity_load_single('file', $image['fid']);
       $file->field_file_image_alt_text[LANGUAGE_NONE][0]['value'] = $alt;
       $file->field_file_image_title_text[LANGUAGE_NONE][0]['value'] = $title;
       $file->field_caption[LANGUAGE_NONE][0]['value'] = $caption;
        
      entity_save('file', $file);
    }
  }
  // Get audio info
  if (isset($row->xml->{'audio-player'})) {
    // Set title
    if (empty($node->title)) {
      $node->title = strip_tags(substr((string) $row->xml->{'id3-title'}, 0, 199));
    }
    // Get image from audio player
    $embed = (string) $row->xml->{'audio-embed'};
    $audioembed = new DOMDocument();
    $audioembed->loadHTML(html_entity_decode($embed));
    // Load iframe source
    $iframe = new DOMXPath($audioembed);
    $srcnode = $iframe->query("//@src")->item(0);
    $src = $srcnode->nodeValue;
    if (strpos($src, 'http') !== FALSE) {
      // Load remote player HTML      
      $embeddedplayer = new DOMDocument();
      $embeddedplayer->loadHTMLFile($src);
      if (!empty($embeddedplayer)) {
        $selector = new DOMXPath($embeddedplayer);
        $img = $selector->query('//img/@src')->item(0);
        $url = $img->nodeValue;
          if (strpos($url, 'http') !== FALSE) {
            // drupal_set_message("Found image " . $url);
            $parsedimage = media_parse_to_file($url);
            // Add the file to the image field of the node. 
            $node->field_image['und'][] = array("fid" => $parsedimage->fid);
          }
       }
    }
 }

 // Get video info
 if (isset($row->xml->{'video-player'})) {
   // If title is empty, set to video caption
   if (empty($node->title)) {
     $node->title = strip_tags(substr((string) $row->xml->{'video-caption'}, 0, 199));
   }
 }
  
 // Save updated node
 global $user;
 $node->uid = $user->uid;
 node_save($node);
 }  

 /**
  * Rollback function
  */
 protected function rollback() {
   parent::rollback();
 }
}
