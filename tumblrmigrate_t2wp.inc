<?php
/**
 * @file
 * T2WP migration class.
 *
 * This class does not require any fields outside of those provided by Drupal's default article node type, and uses only
 * title, body, post date, and tags.  Therefore this class can be used without additional node type configuration.
 * 
 * The Tumblr2Wordpress web application provides a Wordpress-friendly XML feed of all posts,
 * including all basic info easily accessible: title, date, body, tags.  This alone would be suitable for A/V-light blogs.
 * (Thanks go to Hao Chen and Ben Ward for building and maintaining this application.)
 * 
 * The address for the generator is: http://tumblr2wordpress.benapps.net/
 *
 * Note: the mappings below assume that the characters "wp:", which preface element names in T2WP XML files, have been removed for clarity.
 * Make sure to replace "wp:" => "" in this source XML.
 */
class TumblrMigrationT2WP extends XMLMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

  // Migration description
  $this->description = t('XML feed of Tumblr posts');

  // Define XML file source and list of URLs
  $xml_folder = DRUPAL_ROOT . '/' . drupal_get_path('module', 'tumblrmigrate') . '/xml';
  // Define item XPath, relative to document
  $item_xpath = '/rss/channel/item';
  // Define item source ID, relative to item_xpath
  $item_ID_xpath = 'post_name';

    // Fetch multiple files from xml folder
  $urls=array();
  foreach (glob($xml_folder . "/*.xml") as $filename) {
    array_push($urls, $filename);
  }
  
  /**
   * Define explicit fields from XML source, to be used in field mappings below.
   * Reference: https://dgtlmoon.com/drupal-migrate-with-file-field
   */
  $fields = array(
    'title' => t('Title'),
    'sourceid' => t('Post ID from Tumblr'),
    'body' => t('Body'),
    'post_date' => t('Date posted'),  // May need post_date_gmt
    'tags' => t('Tags'),
  );
  
  /**
   * The source ID here is the one retrieved from each data item in the XML file, and
   * used to identify specific items, this is the same structure as the DB fields api
   * Reference: drupal.org/node/1152156
   */
  $this->map = new MigrateSQLMap($this->machineName,
    array(
      'sourceid' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'description' => 'Post ID',
      'alias' => 'p',
       )
    ),
    MigrateDestinationNode::getKeySchema()
  );
  
  // Define source and destination
  // Single URL
  $this->source = new MigrateSourceXML($urls, $item_xpath, $item_ID_xpath, $fields);
  // Destination: Article
  $this->destination = new MigrateDestinationNode('article');

  /**
   * Define field mappings.
   * XPath mappings:
   * addFieldMapping('destinationfield','sourcefield')->xpath('xpath');
   * Apply xpath to source; put value into the source row's 'sourcefield' field.
   * When mappings are processed, the source row's 'sourcefield' field
   * goes into the destination node's 'destinationfield' field.
   * 
   * For example:
   *  $this->addFieldMapping('created', 'created_date')->xpath('/content/CreateDate');
   * 
   * References: drupal.org/node/1152156, https://drupal.la/articles/drupal-migrate-using-xml-in-0-to-35
   */
  $this->addFieldMapping('title', 'title')->xpath('title');
  $this->addFieldMapping('body', 'body')->xpath('content:encoded');
  $this->addFieldMapping('created', 'post_date')->xpath('post_date');
  $this->addFieldMapping('field_tags', 'tags')->xpath('category');
  $this->addFieldMapping('status')->defaultValue(1);
  $this->addFieldMapping('field_tags:create_term')->defaultValue(TRUE); 
  $this->addFieldMapping('field_tags:ignore_case')->defaultValue(TRUE); 
  $this->addFieldMapping('body:language')->defaultValue('en'); 
  $this->addFieldMapping('uid')->defaultValue('0');
  $this->addFieldMapping('body:format', NULL, FALSE)->defaultValue('full_html');
  }
  
  /**
   * Implement a prepareRow method to manipulate the source row before saving
   */
  public function prepareRow($row) {
    return TRUE;
  }
  
  /**
   * Implement a prepare method to manipulate the node information before saving
  */
  public function prepare($node, $row) {
    // Set Node UID to user UID
    global $user;
    $node->uid = $user->uid;
  }

  /**
   * Rollback function
   */
  protected function rollback() {
    parent::rollback();
  }
}
