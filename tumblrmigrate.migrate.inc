<?php
/**
 * @file
 * Declares our migrations.
 */
/**
 * Implements hook_migrate_api(). Setting API level to 2, so that
 * migration classes are recognized by the Migrate module.
 */
function tumblrmigrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'tumblr' => array(
        'title' => t('Tumblr Migrations'),
      ),
    ),
    'migrations' => array(
      'tumblr_api_xml' => array(
        'class_name' => 'TumblrMigrationAPI',
        'group_name' => 'tumblr',
      ),
      'tumblr2wp_xml' => array(
        'class_name' => 'TumblrMigrationT2WP',
        'group_name' => 'tumblr',
      ),
    ),
  );
  return $api;
}
